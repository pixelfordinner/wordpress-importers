<?php
/*
Plugin Name: Pixelfordinner Wordpress Social Media Importer
Plugin URI: https://pixelfordinner.com/
Description: Imports Social Media posts as Wordpress posts.
Version: 1.0
Author: Karl Fathi
Author URI: https://pixelfordinner.com/
License: GPLv2
*/
