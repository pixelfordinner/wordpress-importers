<?php

namespace Pixelfordinner\Wordpress\Instagram;

use Larabros\Elogram\Client as InstagramClient;

class Importer {
    protected $client;
    protected $callbackUrl;
    protected $actions = ['authorize', 'callback'];
    protected $credentials = [];

    protected $route = 'importers/instagram';

    public function __construct($clientId, $clientSecret, $accessToken = NULL)
    {
        // Setup custom route and handling
        $this->setup();

        // Get the callback URL
        $callbackUrl = $this->getCallbackRoute();

        $this->credentials = [
            'client_id'     => $clientId,
            'client_secret' => $clientSecret,
            'access_token'  => $accessToken
        ];

        $accessToken = !is_null($accessToken) ? json_encode(['access_token' => $accessToken]) : NULL;

        $this->client = new InstagramClient($clientId, $clientSecret, $accessToken, $callbackUrl);
        $this->client->secureRequests();
    }

    public function getCallbackRoute() {
        return WP_HOME . '/' . $this->route . '/callback/';
    }

    protected function setup() {
        // Start session
        if (!session_id()) {
            session_start();
        }

        add_action('init', function() {
            // Register route action as a query var
            add_filter('query_vars', function($vars) {
                $vars[] = 'wordpress-instagram-importer';
                return $vars;
            });

            // Register custom route
            add_rewrite_rule(
                '^' . $this->route . '/([a-z]+)/?',
                'index.php?wordpress-instagram-importer=$matches[1]',
                'top'
            );

            // Register custom route handling
            add_action('parse_request', function($wp) {
                if (!empty($wp->query_vars['wordpress-instagram-importer'])) {
                    $this->handleAction($wp->query_vars['wordpress-instagram-importer']);
                    exit(1);
                }
            });
        });
    }

    protected function handleAction($action) {
        if (!in_array($action, $this->actions)) {
            throw new \Exception('Unrecognized action.');
        }

        return call_user_func([$this, $action]);
    }

    protected function authorize() {
        if (!empty($this->credentials['access_token'])) {
            throw new \Exception('Already authorized (an access token was provided)');
        }

        header('Location: '. $this->client->getLoginUrl());
    }

    protected function callback() {
        if (empty($_GET['code'])) {
            throw new \Exception('No access token was returned.');
        }

        $token = $this->client->getAccessToken($_GET['code']);

        echo '<h1>Your instagram access token</h1> ';
        echo '<pre>' . $token->getToken() . '</pre>';
    }

    public function run(int $author, array $categories, array $mediaTypes, string $language, array $translations = [], int $minDate = 0) {
        $importedPosts = 0;

        $data = $this->client->users()->getMedia();
        $collection = $data->get();

        printf("Instagram returned %d items.\n", count($collection));

        if (is_a($collection, 'Illuminate\Support\Collection')) {
            foreach ($data->get() as $item) {
                echo sprintf("\n> Processing item [%d]... ", $item['id']);
                if (in_array($item['type'], $mediaTypes) &&
                    intval($item['created_time']) > $minDate) {
                    printf("\n");
                    $this->createPost($item, $categories, $author, $language, $translations);
                    ++$importedPosts;
                } else {
                    echo "[SKIPPED] (item did not match criteria(s)).\n";
                }
            }
        }

        printf("\nImported %d items from Instagram.\n", $importedPosts);
    }

    protected function createPost($item, $categories, $author, $language, $translations) {
        global $sitepress;

        $post = [
            'post_title'        => sprintf('Instagram Media [%d]', $item['id']),
            'post_author'       => $author,
            'post_category'     => $categories,
            'post_date_gmt'     => date('Y-m-d H:i:s', $item['created_time']),
            'post_modified_gmt' => date('Y-m-d H:i:s', $item['created_time']),
            'post_content'      => $item['caption']['text'],
            'post_status'       => 'publish',
            'meta_input'        => [
                'media'         => 'instagram',
                'id'            => $item['id'],
                'type'          => $item['type'],
                'link'          => $item['link'],
                'images'        => json_encode($item['images'])
            ]
        ];

        // Create base post in default language
        $postData = $this->insertPost($post, $language);

        // Add Translations if necessary
        if (is_int($postData['id']) && is_int($postData['trid']) &&
            is_object($sitepress) && count($translations) > 0) {
            foreach ($translations as $translation) {
                printf("\t\t> Translating post to [%s]... \n", $translation);

                // Get categories translations
                $translatedCategories = [];
                foreach ($categories as $category) {
                    $translatedCategory = icl_object_id($category, 'category', false, $translation);
                    $translatedCategories[] = $translatedCategory;

                    printf("\t\t\t> Category translation [%d => %d]\n", $category, $translatedCategory);
                }

                $post['post_category'] = $translatedCategories;
                printf("\t");

                $translatedPostData = $this->insertPost($post);

                // Link base post and current translation
                if (is_int($translatedPostData['id'])) {
                    printf("\t\t> Setting translation trid [%s]... ", $postData['trid']);
                    $res = $this->translatePost($postData['trid'], $translatedPostData['id'], $language, $translation);
                    echo ($res ? "[SUCCESS] (#{$res})" : "[FAILURE]") . "\n";
                }
            }
        }

        return $postData['id'];
    }

    protected function insertPost(array $post, string $language = '') {
        global $sitepress;
        $trid = NULL;

        printf("\t> Inserting post... ");

        $id = wp_insert_post($post, true);
        echo (is_int($id) ? "[SUCCESS] (#{$id})" : "[FAILURE]") . "\n";

        // Setting post language
        if (is_int($id) && !empty($language)) {
            printf("\t\t> Setting WPML base language [%s]... ", $language);
            $trid = (int)$sitepress->get_element_trid($id);
            $res = $sitepress->set_element_language_details($id, 'post_post', $trid, $language);
            echo ($res ? "[SUCCESS] (#{$trid})" : "[FAILURE]") . "\n";
        }

        if (is_int($id) && class_exists('WPSEO_Meta')) {
          printf("\t\t> Setting no-index to exclude from sitemap...");
          \WPSEO_Meta::set_value('meta-robots-noindex', 1, $id);
        }

        return ['id' => $id, 'trid' => $trid];
    }

    protected function translatePost(int $trid, int $translatedPostId, string $sourceLanguage, string $translationLanguage) {
        global $wpdb;

        return $wpdb->update(
            $wpdb->prefix.'icl_translations',
            [
                'trid' => $trid,
                'element_type' => 'post_post',
                'language_code' => $translationLanguage,
                'source_language_code' => $sourceLanguage
            ],
            [
                'element_id' => $translatedPostId
            ]
        );
    }
}
