<?php

namespace Pixelfordinner\Wordpress\Twitter;

use Abraham\TwitterOAuth\TwitterOAuth as TwitterClient;

class Importer {
    protected $client;
    protected $callbackUrl;
    protected $actions = ['authorize', 'callback'];
    protected $credentials = [];

    protected $route = 'importers/twitter';

    public function __construct($consumerKey, $consumerSecret, $accessToken, $accessTokenSecret)
    {
        $this->credentials = [
            'consumer_key'          => $consumerKey,
            'consumer_secret'       => $consumerSecret,
            'access_token'          => $accessToken,
            'access_token_secret'   => $accessTokenSecret
        ];

        $this->client = new TwitterClient($consumerKey, $consumerSecret, $accessToken, $accessTokenSecret);
    }


    public function run(array $params, int $author, array $categories, string $language, array $translations = [], int $minId = 0) {
        $importedPosts = 0;

        if ($minId > 0) {
            $params['since_id'] = $minId;
        }

        $data = $this->client->get('statuses/user_timeline', $params);

        printf("Twitter returned %d items.\n", count($data));

        if (is_array($data)) {
            foreach ($data as $item) {
                echo sprintf("\n> Processing item [%d]... ", $item->id);
                if (intval($item->id) > $minId) {
                    printf("\n");
                    $this->createPost($item, $categories, $author, $language, $translations);
                    ++$importedPosts;
                } else {
                    echo "[SKIPPED] (item did not match criteria(s)).\n";
                }
            }
        }

        printf("\nImported %d items from Twitter.\n", $importedPosts);
    }

    protected function createPost($item, $categories, $author, $language, $translations) {
        global $sitepress;

        $post = [
            'post_title'        => sprintf('Twitter Media [%d]', $item->id),
            'post_author'       => $author,
            'post_category'     => $categories,
            'post_date_gmt'     => date('Y-m-d H:i:s', strtotime($item->created_at)),
            'post_modified_gmt' => date('Y-m-d H:i:s', strtotime($item->created_at)),
            'post_content'      => $item->text,
            'post_status'       => 'publish',
            'meta_input'        => [
                'media'         => 'twitter',
                'id'            => $item->id
            ]
        ];

        // Create base post in default language
        $postData = $this->insertPost($post, $language);

        // Add Translations if necessary
        if (is_int($postData['id']) && is_int($postData['trid']) &&
            is_object($sitepress) && count($translations) > 0) {
            foreach ($translations as $translation) {
                printf("\t\t> Translating post to [%s]... \n", $translation);

                // Get categories translations
                $translatedCategories = [];
                foreach ($categories as $category) {
                    $translatedCategory = icl_object_id($category, 'category', false, $translation);
                    $translatedCategories[] = $translatedCategory;

                    printf("\t\t\t> Category translation [%d => %d]\n", $category, $translatedCategory);
                }

                $post['post_category'] = $translatedCategories;
                printf("\t");

                $translatedPostData = $this->insertPost($post);

                // Link base post and current translation
                if (is_int($translatedPostData['id'])) {
                    printf("\t\t> Setting translation trid [%s]... ", $postData['trid']);
                    $res = $this->translatePost($postData['trid'], $translatedPostData['id'], $language, $translation);
                    echo ($res ? "[SUCCESS] (#{$res})" : "[FAILURE]") . "\n";
                }
            }
        }

        return $postData['id'];
    }

    protected function insertPost(array $post, string $language = '') {
        global $sitepress;
        $trid = NULL;

        printf("\t> Inserting post... ");

        $id = wp_insert_post($post, true);
        echo (is_int($id) ? "[SUCCESS] (#{$id})" : "[FAILURE]") . "\n";

        // Setting post language
        if (is_int($id) && !empty($language)) {
            printf("\t\t> Setting WPML base language [%s]... ", $language);
            $trid = (int)$sitepress->get_element_trid($id);
            $res = $sitepress->set_element_language_details($id, 'post_post', $trid, $language);
            echo ($res ? "[SUCCESS] (#{$trid})" : "[FAILURE]") . "\n";
        }

        if (is_int($id) && class_exists('WPSEO_Meta')) {
          printf("\t\t> Setting no-index to exclude from sitemap...");
          \WPSEO_Meta::set_value('meta-robots-noindex', 1, $id);
        }

        return ['id' => $id, 'trid' => $trid];
    }

    protected function translatePost(int $trid, int $translatedPostId, string $sourceLanguage, string $translationLanguage) {
        global $wpdb;

        return $wpdb->update(
            $wpdb->prefix.'icl_translations',
            [
                'trid' => $trid,
                'element_type' => 'post_post',
                'language_code' => $translationLanguage,
                'source_language_code' => $sourceLanguage
            ],
            [
                'element_id' => $translatedPostId
            ]
        );
    }
}
